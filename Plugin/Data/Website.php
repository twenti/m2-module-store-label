<?php

namespace Twenti\StoreLabel\Plugin\Data;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_StoreLabel
 */

use Magento\Store\API\Data\WebsiteInterface;


class Website
{

    const PATH_CUSTOM_LABEL = 'general/custom/label';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * Constructor
     *
     * @param  \Magento\Framework\App\Config\ScopeConfigInterface  $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param  \Magento\Store\API\Data\WebsiteInterface  $subject
     * @param  string                                    $result
     * @return string
     */
    public function afterGetName( WebsiteInterface $subject, $result )
    {
        $label = $this->_scopeConfig->getValue(
            self::PATH_CUSTOM_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
            (int) $subject->getId()
        );

        return $label ?: $result;
    }
}
