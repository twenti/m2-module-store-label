<?php

namespace Twenti\StoreLabel\Plugin\Data;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_StoreLabel
 */

use Magento\Store\API\Data\StoreInterface;


class Store
{

    const PATH_CUSTOM_LABEL = 'general/custom/label';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * Constructor
     *
     * @param  \Magento\Framework\App\Config\ScopeConfigInterface  $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param  \Magento\Store\API\Data\StoreInterface  $subject
     * @param  string                                  $result
     * @return string
     */
    public function afterGetName( StoreInterface $subject, $result )
    {
        $label = $this->_scopeConfig->getValue(
            self::PATH_CUSTOM_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            (int) $subject->getId()
        );

        return $label ?: $result;
    }
}
