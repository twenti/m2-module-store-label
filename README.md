# Magento 2 Store Label Module #

---


Allow to set a different store name to show on frontend.


## Compatibility ##
  * Magento Community Edition or Enterprise Edition 2.2.x-2.3.x


## Installing via composer ##
  * Open command line
  * Using command "cd" navigate to your Magento 2 root directory
  * Run commands:
    * ```composer require twenti/m2-module-store-label```
    * ```php bin/magento setup:upgrade```
    * ```php bin/magento setup:di:compile```
    * ```php bin/magento setup:static-content:deploy```


## Support ##
For any issues, please open a support ticket
[issue tracker](https://bitbucket.org/twenti/m2-module-store-label/issues).


## Need More Features ? ##
Please contact us to get a quote
https://twenti.io/contact


## Other extensions by Twenti ##
  * Coming soon
